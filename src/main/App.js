import React from 'react';
import './App.css';

import 'primeflex/primeflex.css';
import 'antd/dist/antd.css';
import 'react-google-places-autocomplete/dist/assets/index.css';

import Routes from './Routes';

function App() {
  return (
    <Routes/>
  );
}

export default App;
