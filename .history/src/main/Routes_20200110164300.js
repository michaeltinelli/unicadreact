import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect, } from 'react-router-dom'
import Layout from '../Layout/Layout'

import NewDeliver from '../pages/NewDeliver';
import Delivers from '../pages/Delivers'
import { Map } from '../pages/Map';

function RouteWithLayout({layout, component, ...rest}){
    return (
      <Route exact {...rest} render={(props) =>
        React.createElement( layout, props, React.createElement(component, props)) 
      }/>
    );
}

export default function Routes() {
    return(
        <Router>
          <Switch> 
            <RouteWithLayout path='/novo' layout={(props) => <Layout {...props} 
            title={'Cadastro'} option={'Inicio'} />} component={NewDeliver} />

            <RouteWithLayout path='/home' layout={(props) => <Layout {...props} 
            title={'Inicio'} option={'Novo'} />} component={Delivers} />

            <RouteWithLayout path='/map/{:id}' layout={(props) => <Layout {...props} 
            title={'Trajeto'} />} component={Map} />
            

            <Redirect to="/home" />
          </Switch>
        </Router>
    )
}