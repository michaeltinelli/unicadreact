import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect, } from 'react-router-dom'
import Layout from '../Layout/Layout'

import NewDeliver from '../pages/NewDeliver';
import Delivers from '../pages/Delivers'

function RouteWithLayout({layout, component, ...rest}){
    return (
      <Route exact {...rest} render={(props) =>
        React.createElement( layout, props, React.createElement(component, props)) 
      }/>
    );
}

export default function Routes() {
    return(
        <Router>
          <Switch> 
            <RouteWithLayout path='/cadastro' layout={(props) => <Layout {...props} 
            title={'Cadastro'} />} component={NewDeliver} />
            <RouteWithLayout path='/home' layout={(props) => <Layout {...props} 
            title={'Inicio'} option={'Cadastro'} />} component={Delivers} />

            

            <Redirect to="/home" />
          </Switch>
        </Router>
    )
}