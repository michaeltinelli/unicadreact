import React from 'react';
import './App.css';
import 'primeflex/primeflex.css';
import 'antd/dist/antd.css';
import Routes from './Routes';

function App() {
  return (
    <Routes/>
  );
}

export default App;
