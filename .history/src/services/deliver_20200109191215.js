import axios from 'axios'

const baseUrl = 'http://localhost:8000'

export function save(delivery) {
    return axios.post(`${baseUrl}/delivery`, delivery)
}