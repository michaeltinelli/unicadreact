import axios from 'axios'

const baseUrl = 'https://tranquil-bastion-33158.herokuapp.com/api'

export function save(delivery) {
    console.log(delivery)
    return axios.post(`${baseUrl}/deliveries`, delivery )
}

export function getDeliveries() {
    return axios.get(`${baseUrl}/deliveries`)
}

export function getDelivery(id) {
    return axios.get(`${baseUrl}/delivery/${id}`)
}