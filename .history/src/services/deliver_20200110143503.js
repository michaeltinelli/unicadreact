import axios from 'axios'

const baseUrl = 'http://127.0.0.1:8000/api'

export function save(delivery) {
    console.log(delivery)
    return axios.post(`${baseUrl}/deliveries`, JSON.parse(delivery))
}

export function getDeliveries() {
    return axios.get(`${baseUrl}/deliveries`)
}