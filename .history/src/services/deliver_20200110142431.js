import axios from 'axios'

const baseUrl = 'http://127.0.0.1:8000/api'

export function save(delivery) {
    return axios.post(`${baseUrl}/deliveries`, {...delivery})
}

export function getDeliveries() {
    return axios.get(`${baseUrl}/deliveries`)
}