import axios from 'axios'

const baseUrl = 'http://127.0.0.1:8000'

export function save(delivery) {
    return axios.post(`${baseUrl}/delivery`, delivery)
}