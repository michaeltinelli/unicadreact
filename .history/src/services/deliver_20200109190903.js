import axios from 'axios'

const baseUrl = 'localhost:8000'

export function save(delivery) {
    return axios.post(`${baseUrl}/delivery`, delivery)
}