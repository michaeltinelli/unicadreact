import React from 'react'
import { Input, DatePicker, Icon } from 'antd'

export const InputDefault = ({ label, flexGrid, icon, }) => {
    return(
        <div className={flexGrid}>
            <div className={'contentInput'}>
            <Input prefix={<Icon type={icon} />} placeholder={label} />
            </div>
        </div>
    )
}

export const InputDate = ({ flexGrid, onChange }) => {
    return(
        <div className={'contentInput'}>
            <DatePicker onChange={onChange} className={flexGrid} />
        </div>
    )
}