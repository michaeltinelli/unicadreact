import React from 'react'
import { Input, DatePicker, Icon } from 'antd'
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';

export const InputDefault = ({ label, flexGrid, icon, setObject}) => {
    return(
        <div className={[flexGrid]}>
            <Input prefix={<Icon type={icon} />} placeholder={label}
            onChange={e => setObject(e.target.value)}/>
        </div>
    )
}

export const InputDate = ({ flexGrid, onChange }) => {
    return(
        <div className={flexGrid}>
            <DatePicker onChange={onChange} format={'DD/MM/YYYY'}  />
        </div>
    )
}

export const AutoComplete = ({ label, flexGrid, icon, setObject }) => {
    return(
        <div className={[flexGrid]}>
            <GooglePlacesAutocomplete onSelect={({ description }) => setObject(description)} autocompletionRequest={{
                componentRestrictions: { country: 'br'},
            }} inputStyle={{ width: '90%', borderRadius: '4px'}} placeholder={label} />
        </div>
    )
}

export const InputRead = ({ label, flexGrid, value }) => {
    return(
        <div className={[flexGrid]}>
            <Input placeholder={label} readOnly value={value}/>
        </div>
    )
}