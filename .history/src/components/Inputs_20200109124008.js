import React from 'react'
import { Input } from 'antd'
export const InputDefault = ({ label, flexGrid }) => {
    return(
        <div className={flexGrid}>
           <Input  placeholder={label} />
        </div>
    )
}