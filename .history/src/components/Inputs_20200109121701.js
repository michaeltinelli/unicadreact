import React from 'react'

export const InputDefault = ({ label, flexGrid }) => {
    return(
        <div className={flexGrid}>
            <div id="containerInput">
                <label>{label}</label>
                <input id={'input'} />
            </div>
        </div>
    )
}