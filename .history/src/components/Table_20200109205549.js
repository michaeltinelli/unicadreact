import React from 'react'
import { Table } from 'antd'

export default ({ data, columns}) => {
    return(
        <Table dataSource={data} columns={columns} />
    )
}