import React from 'react'
import { Input, DatePicker } from 'antd'

export const InputDefault = ({ label, flexGrid }) => {
    return(
        <div className={flexGrid}>
           <Input  placeholder={label} />
        </div>
    )
}

export const InputDate = ({ flexGrid, onChange }) => {
    return(
        <DatePicker onChange={onChange} className={flexGrid} />
    )
}