import React from 'react'
import { Input, DatePicker, Icon } from 'antd'
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';

export const InputDefault = ({ label, flexGrid, icon, }) => {
    return(
        <div className={[flexGrid]}>
            <Input prefix={<Icon type={icon} />} placeholder={label} className={'contentInput'} />
        </div>
    )
}

export const InputDate = ({ flexGrid, onChange }) => {
    return(
        <div className={flexGrid}>
            <DatePicker onChange={onChange}  />
        </div>
    )
}

export const AutoComplete = ({ label, flexGrid, icon, onGeoLocate }) => {
    return(
        <div className={[flexGrid]}>
            <GooglePlacesAutocomplete autocompletionRequest={{
                componentRestrictions: { country: ['br']},
            }} inputStyle={{ width: '50%'}}/>
        </div>
    )
}