import React from 'react'
import { Table } from 'antd'
const { Column } = Table

export default ({ data, columns}) => {
    return(
        <Table dataSource={data} columns={columns}>
            <Column title={'Opção'} key={'option'} render={_ => <span>Trajeto</span>} />
        </Table>
    )
}