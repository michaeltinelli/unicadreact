import React from 'react'
import { Input, DatePicker, Icon } from 'antd'

export const InputDefault = ({ label, flexGrid, icon, }) => {
    return(
        <div className={[flexGrid]}>
            <Input prefix={<Icon type={icon} />} placeholder={label} />
        </div>
    )
}

export const InputDate = ({ flexGrid, onChange }) => {
    return(
        <DatePicker onChange={onChange} className={[flexGrid]} />
    )
}