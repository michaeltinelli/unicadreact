import React from 'react'
import { Layout as LayoutAntd } from 'antd'
import { Link } from 'react-router-dom'
const { Header, Content } = LayoutAntd

export default function Layout({ children, title, option }) {
    return(
        <div >
            <Header className='header' style={{backgroundColor: '#0481c4'}}>
                <h2 id={'title'}>{title}</h2>

                <Link to={`/${option.toLowerCase()}`}>{option}</Link>
            </Header>

            <Content className='content'>
                {children}
            </Content>
        </div>
    )
}