import React from 'react'
import { Layout as LayoutAntd } from 'antd'
const { Header, Content } = LayoutAntd
export default function Layout({ children, title }) {
    return(
        <div >
            <Header className='header' style={{backgroundColor: '#0481c4'}}>
                <h2 id={'title'}>{title}</h2>
            </Header>

            <Content className='content'>
                {children}
            </Content>
        </div>
    )
}