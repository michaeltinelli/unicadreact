import React from 'react'

export default function Layout({ children, title }) {
    return(
        <div className='containerLayout'>
            <div className='header'>
                <h2>{title}</h2>

            </div>

            <div className='content'>
                {children}
            </div>
        </div>
    )
}