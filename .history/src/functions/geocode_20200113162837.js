import Geocode from "react-geocode";

function initialize() {
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')
}

export async function  getPositions(address) {
    //await initialize()

    const resp = await Geocode.fromAddress(address)
    const { lat, lng } = resp.results[0].geometry.location
    //const latParsed = parseFloat(lat).toFixed(3)
    //const lngParsed = parseFloat(lng).toFixed(3)
   

   console.log(resp)

    return { lat, lng }
}