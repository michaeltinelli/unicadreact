import Geocode from "react-geocode";

function initialize() {
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')
}

export async function  getPositions(address) {
    await initialize()

    const resp = await Geocode.fromAddress(address)
    const { lat, lng } = resp.results[0].geometry.location
    console.log(lat,lng)
    return { lat, lng }
}