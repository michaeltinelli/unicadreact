import Geocode from "react-geocode";

function initialize() {
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')
}

export async function  getPositions(address) {
    await initialize()

    const resp = await Geocode.fromAddress(address)
    return resp[0].geometry.location
}