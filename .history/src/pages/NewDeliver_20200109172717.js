import React, { useState } from 'react'
import { InputDefault, InputDate, AutoComplete } from '../components/Inputs'

export default function NewDeliver({ }) {

    const [delivery, setDelivery] = useState({
        name: '',
        dateDel: null,
        origin: '',
        destiny: '',
    })

    function onChangeDate(date) {
        setDelivery({...delivery, dateDel: date})
    }

   

    return(
        <div className={['p-grid']}>
            <div className={'page'}>
                <InputDefault label={'Cliente'} icon={'user'} flexGrid={'p-col-12 p-md-6 p-lg-4'} />
                <InputDate flexGrid={'p-col-12 p-md-6 p-lg-4'} onChange={onChangeDate}/>
                <AutoComplete label={'Origem'} flexGrid={'p-col-12 p-md-6 p-lg-4'} />
                <AutoComplete label={'Destino'} flexGrid={'p-col-12 p-md-6 p-lg-4'}/>

            </div>
        </div>
    )
}