import React, { useState } from 'react'
import { InputDefault, InputDate, AutoComplete } from '../components/Inputs'

export default function NewDeliver({ }) {

    const [delivery, setDelivery] = useState({
        name: '',
        dateDel: null,
    })

    const [origin, setOrigin] = useState('')
    const [destiny, setDestiny] = useState('')

    function onChangeDate(date) {
        setDelivery({...delivery, dateDel: date})
    }

    return(
        <div className={['p-grid']}>
            <div className={'page'}>
                <div className={'p-md-offset-2 p-lg-6'}></div>

                <InputDefault label={'Cliente'} icon={'user'} flexGrid={'p-col-12 p-md-5 p-lg-6'} />
                <InputDate flexGrid={'p-col-12 p-md-5 p-lg-6'} onChange={onChangeDate}/>

                <AutoComplete label={'Origem'} flexGrid={'p-col-12 p-md-5 p-lg-6'} 
                 setObject={setOrigin} />

                <AutoComplete label={'Destino'} flexGrid={'p-col-12 p-md-5 p-lg-6'}  
                 setObject={setDestiny} />

                <div className={'p-md-offset-2 p-lg-6'}></div>
            </div>
        </div>
    )
}