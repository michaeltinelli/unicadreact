import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';

import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';
import { getPositions } from '../functions/geocode';

const Marker = ({ text }) => <div>{text}</div>;

export function Map({ match: { params: { id } } }) {
    
    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState({})
    const [destinyPosition, setDestinyPosition] = useState({})
    const [center, setCenter] = useState({})

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) {
               setDelivery(resp.data)
               setOriginPosition(await getPositions(resp.data.origin))
               setDestinyPosition(await getPositions(resp.data.destiny))
           }   
           
       }
       get()
    }, [])

    const renderMap = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.origin} label={'Origem'} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />

            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} label={'Destino'}/>
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <div style={{ height: '100%', width: '100%', border: '1px solid black' }}>
                    <GoogleMapReact defaultZoom={11} bootstrapURLKeys={{ key: 'AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok' }} >
                        <Marker text={'Origem'} lat={originPosition.lat} lng={originPosition.lng} />
                        <Marker text={'Destino'} lat={destinyPosition.lat} lng={destinyPosition.lng} />
                    </GoogleMapReact>
            </div> 
        </>
    )

    

    return(
        <div className='p-grid'>
            {
                delivery && renderMap()
            }
        </div>
    )
}