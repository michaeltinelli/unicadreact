import React, { useState, useEffect } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';
import { getPositions } from '../functions/geocode';


function Map({ match: { params: { id } } }) {
    
    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState(null)
    const [destinyPosition, setDestinyPosition] = useState(null)
    

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) {
                setDelivery(resp.data)
                setOriginPosition(await getPositions(resp.data.origin))
                setDestinyPosition(await getPositions(resp.data.destiny))
           }   
           
       }
       get()
    }, [])

    const renderMap = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.origin} label={'Origem'} />
            <div className={'p-md-offset-1 p-lg-offset-1'} />

            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} label={'Destino'}/>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            
            
            <MyMapComponent originPosition={originPosition} destinyPosition={destinyPosition} 
            loadingElement={<div style={{ height: `100%`, width: '100%' }} />} 
            containerElement={<div style={{ height: `100%`, width: '100%' }} />}
            mapElement={<div style={{ height: `150%`, width: '100%' }} />} isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok&v=3.exp&libraries=geometry,drawing,places"
            />
        </>
    )

    

    return(
        <div className={'p-col-12'}>
            {
                delivery && originPosition && destinyPosition && renderMap()
            }
        </div>
    )
}

const MyMapComponent = withScriptjs( withGoogleMap(({ originPosition, destinyPosition, ...rest}) => {
    console.log(originPosition)
    return (
    <GoogleMap 
        defaultZoom={14}
        defaultCenter={originPosition}
        {...rest}
    >
        <Marker position={originPosition} defaultLabel={'Origem'} />
        <Marker position={destinyPosition} defaultLabel={'Destino'}/>
    </GoogleMap>
    )
}))

export default Map