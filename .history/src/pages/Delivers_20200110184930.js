import React, { useEffect, useState } from 'react'
import { getDeliveries } from '../services/deliver'
import { Table, Button } from 'antd'
const { Column } = Table

export default  function Delivers({ history }) {

    const [list, setList] = useState([])

    useEffect(() => {
       getDeliveries().then(resp => {
           console.log(resp)
           setList(resp.data)
       })
    }, [])

    return(
        <div className={'p-grid'}>
            <div className={'p-col-12'}>
                <Table dataSource={list} rowKey={'delivery'} style={{backgroundColor: '#fff'}}>
                    <Column title={'Cliente'} key={'name'} dataIndex={'name'} />
                    <Column title={'Mapa'} render={props => {
                        console.log(props)
                        return(<Button type={'link'} onClick={_ => 
                            history.push(`/map/${props.id}`)}>Acessar</Button>)
                    }}  />
                </Table>           
            </div>
        </div>
    )
}