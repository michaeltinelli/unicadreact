import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';
import Geocode from "react-geocode";
import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';

export function Map({ match: { params: { id } } }) {
    
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')

    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState(null)
    const [destinyPosition, setDestinyPosition] = useState(null)

    useEffect(() => {
       
        getDelivery(id).then(resp => {
            if(resp.status === 200) {
                setDelivery(resp.data)
                getPositions()
            }
        })

    }, [])

    const renderAddresses = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'}  value={delivery.origin} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />

            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />
        </>
    )

    async function getPositions() {
        const respOrigin = await Geocode.fromAddress(delivery.origin)
        const respDestiny = await Geocode.fromAddress(delivery.destiny)
        
        console.log(respOrigin)
        console.log(respDestiny)
    }

    return(
        <div className='p-grid'>
            {
                renderAddresses()
            }
            <GoogleMapReact>

            </GoogleMapReact>
        </div>
    )
}