import React, { useState } from 'react'
import { InputDefault, InputDate, AutoComplete } from '../components/Inputs'
import { Button } from 'antd'
import { save } from '../services/deliver'

export default function NewDeliver({ }) {

    const [name, setName] = useState('')

    const [dateDel, setDateDel] = useState(null)

    const [origin, setOrigin] = useState('')
    const [destiny, setDestiny] = useState('')

    function onChangeDate(date, dateString) {
        setDateDel({date: dateString})
    }

    const resetFields = () => {
        setName('')
        setDateDel(null)
        setOrigin('')
        setDestiny('')
    }

    async function onSubmit() {
        const resp = await save({name, dateDel, origin, destiny})
        console.warn(resp.data)
        if(resp.data) {
            resetFields()
        }
    }

    return(
        <div className={['p-grid']}>
            <div className={'page'}>
                <div className={'p-md-offset-2 p-lg-6'}></div>

                <InputDefault label={'Cliente'} icon={'user'} flexGrid={'p-col-12 p-md-5 p-lg-6'} setObject={setName}/>
                <InputDate flexGrid={'p-col-12 p-md-5 p-lg-6'} onChange={onChangeDate}/>

                <AutoComplete label={'Origem'} flexGrid={'p-col-12 p-md-5 p-lg-6'} 
                 setObject={setOrigin} />

                <AutoComplete label={'Destino'} flexGrid={'p-col-12 p-md-5 p-lg-6'}  
                 setObject={setDestiny} />

                <div className={'p-md-offset-2 p-lg-6'}></div>
                <Button onClick={() => onSubmit()} className={'p-col-12 p-md-12 p-lg-12'} type={'primary'}>Salvar</Button>
            </div>
        </div>
    )
}