import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';

import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';
import { getPositions } from '../functions/geocode';

const Marker = ({ text }) => <div>{text}</div>;

export function Map({ match: { params: { id } } }) {
    
   

    const [delivery, setDelivery] = useState({})
    const [originPosition, setOriginPosition] = useState({})
    const [destinyPosition, setDestinyPosition] = useState({})

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) {
               await setDelivery(resp.data)
               await setOriginPosition(await getPositions(resp.data.origin))
               await setDestinyPosition(await getPositions(resp.data.destiny))
           }   
           
       }
       get()
    }, [])

    const renderAddresses = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'}  value={delivery.origin} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />

            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            
        </>
    )

    

    return(
        <div className='p-grid'>
            {
                delivery && renderAddresses()
            }
            <GoogleMapReact defaultZoom={11} >
                <Marker text={delivery.origin} lat={originPosition.lat} lng={originPosition.lng} />
            </GoogleMapReact>
        </div>
    )
}