import React, { useEffect, useState } from 'react'
import { getDeliveries } from '../services/deliver'
import { Table } from 'antd'
const { Column } = Table

export default  function Delivers({ }) {

    const [list, setList] = useState([])

    useEffect(() => {
       getDeliveries().then(resp => setList(resp.data))
    }, [])

    return(
        <div className={'p-grid'}>
            <div className={'p-col-12'}>
                <Table dataSource={list}>
                    <Column title={'Cliente'} key={'name'} dataIndex={'name'} />
                    <Column title={'Mapa'} key={'map'} render={props => {
                        console.log(props)
                        return(<span>Trajeto</span>)
                    }}  />
                </Table>           
            </div>
        </div>
    )
}