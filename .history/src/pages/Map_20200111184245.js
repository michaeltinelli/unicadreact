import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';

import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';
import { getPositions } from '../functions/geocode';

const Marker = ({ text }) => <div>{text}</div>;

export function Map({ match: { params: { id } } }) {
    
    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState({})
    const [destinyPosition, setDestinyPosition] = useState({})
    const [center, setCenter] = useState({})

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) {
               setDelivery(resp.data)
               setOriginPosition(await getPositions(resp.data.origin))
               setDestinyPosition(await getPositions(resp.data.destiny))
               setCenter(originPosition)
           }   
           
       }
       get()
    }, [])

    const renderMap = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.origin} label={'Origem'} />
            <div className={'p-md-offset-1 p-lg-offset-1'} />

            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} label={'Destino'}/>
            <div className={'p-md-offset-1 p-lg-offset-1'} />

            <GoogleMapReact style={{ width: '100vh', height: '100vh'}}  defaultZoom={11} center={center} bootstrapURLKeys={{ key: 'AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok' }} >
                
            </GoogleMapReact>
             
        </>
    )

    

    return(
        <div className='p-grid'>
            {
                delivery && renderMap()
            }
        </div>
    )
}