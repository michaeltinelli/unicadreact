import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';
import Geocode from "react-geocode";
import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';

export function Map({ match: { params: { id } } }) {
    
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')

    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState(null)
    const [destinyPosition, setDestinyPosition] = useState(null)

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) setDelivery(resp.data)
       }
       get()
    }, [])

    const renderAddresses = () => (
        <>
            <InputRead flexGrid={'p-col-12 p-md-12 p-lg-3'}  value={delivery.origin} />
            <InputRead flexGrid={'p-col-12 p-md-12 p-lg-3'} value={delivery.destiny} />
        </>
    )

    return(
        <div className='p-grid'>
            {
                delivery && renderAddresses()
            }
            <GoogleMapReact>

            </GoogleMapReact>
        </div>
    )
}