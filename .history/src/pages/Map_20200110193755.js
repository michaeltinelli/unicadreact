import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';
import Geocode from "react-geocode";
import { getDelivery } from '../services/deliver';

export function Map({ match: { params: { id } } }) {
    
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           console.log(resp)
       }
       get()
    }, [])

    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState(null)
    const [destinyPosition, setDestinyPosition] = useState(null)

    return(
        <div className='p-grid'>
            <GoogleMapReact>

            </GoogleMapReact>
        </div>
    )
}