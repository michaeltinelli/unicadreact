import React, { useState, useEffect } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer } from "react-google-maps"
import { Button } from 'antd'
import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';
import { getPositions } from '../functions/geocode';



function Map({ history, match: { params: { id } } }) {
    
    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState(null)
    const [destinyPosition, setDestinyPosition] = useState(null)
    

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) {
                setDelivery(resp.data)
                setOriginPosition(await getPositions(resp.data.origin))
                setDestinyPosition(await getPositions(resp.data.destiny))
           }   
           
       }
       get()
    }, [])

    const renderMap = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.origin} label={'Origem'} />
            <div className={'p-md-offset-1 p-lg-offset-1'} />

            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} label={'Destino'}/>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            
            
            <MyMapComponent originPosition={originPosition} destinyPosition={destinyPosition} 
            loadingElement={<div style={{ height: `100%`, width: '100%' }} />} 
            containerElement={<div style={{ height: `100%`, width: '100%' }} />}
            mapElement={<div style={{ height: `200%`, width: '100%' }} />} isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok&v=3.exp&libraries=geometry,drawing,places"
            />
        </>
    )


    return(
        <div className={'p-col-12'}>
            <Button className={'p-col-12'} type={'ghost'} onClick={() => history.push('/home')}>Voltar</Button>
            {
                delivery && originPosition && destinyPosition && renderMap()
            }
        </div>
    )
}

const MyMapComponent = withScriptjs( withGoogleMap(({ originPosition, destinyPosition, ...rest}) => {
    const [directions, setDirections] = useState(null)
    const google = window.google
    const directionsService = new google.maps.DirectionsService();

    directionsService.route(
        {
          origin: originPosition,
          destination: destinyPosition,
          travelMode: google.maps.TravelMode.DRIVING
        },
        (result, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            setDirections(result)
          }
        }
      );
    
    return (
    <GoogleMap 
        defaultZoom={14}
        defaultCenter={originPosition}
        {...rest}
    >
        <DirectionsRenderer  directions={directions}/>
    </GoogleMap>
    )
}))

export default Map