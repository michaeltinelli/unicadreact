import React, { useState } from 'react'
import { InputDefault, InputDate, AutoComplete } from '../components/Inputs'

export default function NewDeliver({ }) {

    const [delivery, setDelivery] = useState({
        name: '',
        dateDel: null,
        origin: '',
        destiny: '',
    })

    function onChangeDate(date) {
        setDelivery({...delivery, dateDel: date})
    }

    function geoLocate() {
        
    }

    return(
        <div className={['p-grid', 'contentPage']}>
            <InputDefault label={'Cliente'} icon={'user'} flexGrid={'p-col-12 p-md-6 p-lg-4'} />
            <InputDate flexGrid={'p-col-12 p-md-6 p-lg-4'} onChange={onChangeDate}/>
            <AutoComplete />
        </div>
    )
}