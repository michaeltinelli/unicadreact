import React, { useState, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';
import Geocode from "react-geocode";
import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';

export function Map({ match: { params: { id } } }) {
    
    Geocode.setApiKey('AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok')
    Geocode.setLanguage('en')
    Geocode.setRegion('br')

    const [delivery, setDelivery] = useState({})
    const [originPosition, setOriginPosition] = useState({})
    const [destinyPosition, setDestinyPosition] = useState({})

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) setDelivery({...resp.data})
           
       }
       get()
    }, [])

    const renderAddresses = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'}  value={delivery.origin} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />

            <div className={'p-md-offset-1 p-lg-offset-2'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} />
            <div className={'p-md-offset-1 p-lg-offset-2'} />
            { getPositions()}
        </>
    )

    async function getPositions() {
        const respOrigin = await Geocode.fromAddress(delivery.origin)
        const respDestiny = await Geocode.fromAddress(delivery.destiny)
        
        console.log(respOrigin[0])
        console.log(respDestiny[0])
    }

    return(
        <div className='p-grid'>
            {
                delivery && renderAddresses()
            }
            <GoogleMapReact>

            </GoogleMapReact>
        </div>
    )
}