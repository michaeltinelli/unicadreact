import React, { useEffect, useState } from 'react'
import { getDeliveries } from '../services/deliver'
import { Table } from 'antd'
const { Column } = Table

export default  function Delivers({ }) {

    const [list, setList] = useState([])

    useEffect(() => {
       getDeliveries().then(resp => setList(resp.data))
    }, [])

    return(
        <div className={'p-grid'}>
            <div className={'p-col-12'}>
                <Table dataSource={list}>
                    <Column title={'Cliente'} key={(e,i) => i} dataIndex={'name'} />
                    <Column title={'Mapa'} key={(e,i) => i} render={props => {
                        console.log(props)
                        return(<span>Trajeto</span>)
                    }}  />
                </Table>           
            </div>
        </div>
    )
}