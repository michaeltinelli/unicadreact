import React, { useEffect, useState } from 'react'
import Table from '../components/Table'
import { getDeliveries } from '../services/deliver'

export default  function Delivers({ }) {

    const [list, setList] = useState([])

    useEffect(() => {
       getDeliveries().then(resp => setList(resp.data))
    }, [])

    return(
        <div className={'p-grid'}>
            <div className={'p-col-12'}>
                <Table data={list} />                
            </div>
        </div>
    )
}