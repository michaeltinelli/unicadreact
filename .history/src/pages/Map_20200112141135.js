import React, { useState, useEffect } from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

import { getDelivery } from '../services/deliver';
import { InputRead } from '../components/Inputs';
import { getPositions } from '../functions/geocode';


function Map({ match: { params: { id } } }) {
    
    const [delivery, setDelivery] = useState(null)
    const [originPosition, setOriginPosition] = useState(null)
    const [destinyPosition, setDestinyPosition] = useState(null)
    const [center, setCenter] = useState(null)

    useEffect(() => {
       async function get() {
           const resp = await getDelivery(id)
           if(resp.status === 200) {
               setDelivery(resp.data)
               setOriginPosition(await getPositions(resp.data.origin))
               setDestinyPosition(await getPositions(resp.data.destiny))
               setCenter(originPosition)
           }   
           
       }
       get()
    }, [])

    const renderMap = () => (
        <>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.origin} label={'Origem'} />
            <div className={'p-md-offset-1 p-lg-offset-1'} />

            <div className={'p-md-offset-1 p-lg-offset-1'} />
            <InputRead flexGrid={'p-col-12 p-md-10 p-lg-10'} value={delivery.destiny} label={'Destino'}/>
            <div className={'p-md-offset-1 p-lg-offset-1'} />
            
            
            <MyMapComponent center={center} originPosition={originPosition} destinyPosition={destinyPosition} 
            loadingElement={<div style={{ height: `100%` }} />} containerElement={<div style={{ height: `400px` }} />}
            mapElement={<div style={{ height: `100%` }} />} isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxwjyj3Hf86Zh8xaGczXhKrYj28Jp73Ok&libraries=places"
            />
        </>
    )

    

    return(
        <div className='p-grid'>
            {
                delivery && renderMap()
            }
        </div>
    )
}

const MyMapComponent = withScriptjs( withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={11}
    defaultCenter={props.center}
    {...props.rest}
  >
      {console.warn(props)}
    <Marker position={props.originPosition}/>
  </GoogleMap>
))

export default Map